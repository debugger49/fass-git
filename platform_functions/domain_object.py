from xpms_helper.model import domain_util,document_util,recommendation_util


def domain_object(config, **kwargs):
    doc_id = config["context"]["doc_id"]
    solution_id = config["context"]["solution_id"]
    payload = {"solution_id": solution_id, "doc_id": doc_id, "root_id": doc_id}
    domain = domain_util.get_domain_object(payload)
    return {"domain": domain}
